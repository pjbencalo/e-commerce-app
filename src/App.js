import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

// components
import AppNavbar from "./components/AppNavbar";

// pages
import About from "./pages/About";
import AllProducts from "./pages/AllProducts";
import Cart from "./pages/Cart";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import MyAccount from "./pages/MyAccount";
import MyOrders from "./pages/MyOrders";
import Orders from "./pages/Orders";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Register from "./pages/Register";
import Users from "./pages/Users";

// css
import "./App.css";

import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({ id: null, isAdmin: null });
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        })
        .catch((error) => {});
    }
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container>
          <AppNavbar />

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/myAccount" element={<MyAccount />} />
            <Route path="/myOrders" element={<MyOrders />} />
            <Route path="/orders" element={<Orders />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/all" element={<AllProducts />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/users" element={<Users />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
