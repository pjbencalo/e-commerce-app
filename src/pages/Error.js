import { NavLink } from "react-router-dom";
import "./Pages.css";

export default function Error() {
  return (
    <section class="page_404">
      <div class="container">
        <div class="row mt-5">
          <div class="col-sm-12">
            <div class="col-sm-10 col-sm-offset-1 text-center w-100">
              <div class="four_zero_four_bg">
                <h1 class="text-center ">404</h1>
              </div>

              <div class="content_box_404">
                <h3 class="h2">Looks like you're lost</h3>

                <p>The page you are looking for is not available!</p>

                <NavLink
                  to="/"
                  className="link_404 text-decoration-none red-button"
                >
                  Go to Home
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
