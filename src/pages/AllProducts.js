import { useContext, useEffect, useState, useRef } from "react";
import { Button, Card, Col, Form, Modal, Row, Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

import "./Pages.css";

function AllProducts() {
  const { user } = useContext(UserContext);
  // for adding products
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [image, setImage] = useState(null);
  // button will activate once condition is met
  const [isActive, setIsActive] = useState(false);
  // for getting all the products
  const [products, setProducts] = useState([]);
  // for resetting image input
  const fileInputRef = useRef(null);

  const handleImageChange = (event) => {
    const selectedFile = event.target.files[0];
    setImage(selectedFile);
  };

  // Collapse modal for update product
  const [selectedProduct, setSelectedProduct] = useState(null);
  const handleEditClick = (productId) => {
    const product = products.find((product) => product._id === productId);
    setSelectedProduct(product);
    handleShowModal();
  };

  // Updating product
  function updateProduct(e) {
    e.preventDefault();
    fetch(
      `${process.env.REACT_APP_API_URL}/products/${selectedProduct._id}/update`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: selectedProduct.name,
          description: selectedProduct.description,
          price: selectedProduct.price,
          quantity: selectedProduct.quantity,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          const updatedProducts = products.map((product) => {
            if (product._id === selectedProduct._id) {
              return {
                ...product,
                name: selectedProduct.name,
                description: selectedProduct.description,
                price: selectedProduct.price,
                quantity: selectedProduct.quantity,
              };
            }
            return product;
          });

          setProducts(updatedProducts);

          Swal.fire({
            icon: "success",
            title: "Successfully updated!",
          });

          handleCloseModal();
        } else {
          Swal.fire({
            icon: "error",
            title: `Something is wrong!`,
          });
        }
      })
      .catch((error) => {});
  }

  // Display All products
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {});
  }, []);

  // close modal
  const [showModal, setShowModal] = useState(false);

  const handleCloseModal = () => {
    setShowModal(false);
  };

  // show modal
  const handleShowModal = () => {
    setShowModal(true);
  };

  // Collapsible for add product
  const [isCollapsed, setIsCollapsed] = useState(true);

  const toggleCollapse = () => {
    setIsCollapsed(!isCollapsed);
  };

  // Disable Product

  const disableProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId
              ? { ...product, isActive: false }
              : product
          )
        );
      })
      .catch((err) => {});
  };

  // Enable product
  const enableProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: true } : product
          )
        );
      })
      .catch((err) => {});
  };

  // Add Product
  const newProduct = (e) => {
    e.preventDefault();
    setProductName("");
    setDescription("");
    setPrice("");
    setQuantity("");
    setImage(null);
    if (fileInputRef.current) {
      fileInputRef.current.value = ""; // Reset the file input field value
    }

    fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: productName,
        description: description,
        price: price,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          const formData = new FormData();
          formData.append("image", image);
          fetch(`${process.env.REACT_APP_API_URL}/products/${data._id}/image`, {
            method: "POST",
            body: formData,
          })
            .then((imageData) => {
              if (imageData) {
                Swal.fire({
                  icon: "success",
                  title: `${data.name} is successfully added!`,
                  text: "You may now add new product.",
                });
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Ooops! Something went wrong!",
                  text: "Please try to add the product again and check the fields properly.",
                });
              }
            })
            .catch((error) => {});
        } else {
          Swal.fire({
            icon: "error",
            title: `${productName} is already registered!`,
            text: "Add a different product name.",
          });
        }
      })
      .catch((error) => {});
  };

  useEffect(() => {
    if (
      productName !== "" &&
      description !== "" &&
      price !== "" &&
      quantity !== "" &&
      image !== null
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, price, quantity, image]);

  return !user.id || !user.isAdmin ? (
    <Navigate to="/products" />
  ) : (
    <Row className="row-cols-12 min-vh-100 ">
      <Col className="mt-5 text-center">
        <h1 className="m-5 cart-title">Products</h1>
        <Table striped="columns">
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr key={product._id}>
                <th className="product-name col-sm-2">{product.name}</th>
                <td className="col-sm-2">{product.description}</td>
                <td className="col-sm-1">&#8369; {product.price}</td>
                <td className="col-sm-1">{product.quantity}</td>
                <td className="col-sm-1">
                  <div>
                    <Button
                      onClick={() => handleEditClick(product._id)}
                      className="mx-1 red-button"
                    >
                      Edit
                    </Button>

                    {product.isActive ? (
                      <Button
                        onClick={() => disableProduct(product._id)}
                        className="mx-1 red-button"
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        onClick={() => enableProduct(product._id)}
                        className="mx-1 green-button"
                      >
                        Activate
                      </Button>
                    )}
                  </div>
                </td>
              </tr>
            ))}
            {selectedProduct && (
              <Modal
                show={showModal}
                onHide={handleCloseModal}
                className="update-modal"
              >
                <Modal.Header closeButton>
                  <Modal.Title>Edit Product</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Form onSubmit={(e) => updateProduct(e)}>
                    <Form.Group controlId="updateProductName">
                      <Form.Label>Product Name</Form.Label>
                      <Form.Control
                        className="my-2"
                        type="text"
                        placeholder="Enter product name"
                        value={selectedProduct.name}
                        onChange={(e) =>
                          setSelectedProduct((prevProduct) => ({
                            ...prevProduct,
                            name: e.target.value,
                          }))
                        }
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="updateProductDescription">
                      <Form.Label>Description</Form.Label>
                      <Form.Control
                        className="my-2"
                        as="textarea"
                        rows={3}
                        placeholder="Enter product description"
                        value={selectedProduct.description}
                        onChange={(e) =>
                          setSelectedProduct((prevProduct) => ({
                            ...prevProduct,
                            description: e.target.value,
                          }))
                        }
                        required
                      />
                    </Form.Group>

                    <Form.Group controlId="updateProductPrice">
                      <Form.Label>Price</Form.Label>
                      <Form.Control
                        className="my-2"
                        type="number"
                        placeholder="Enter product price"
                        value={selectedProduct.price}
                        onChange={(e) =>
                          setSelectedProduct((prevProduct) => ({
                            ...prevProduct,
                            price: e.target.value,
                          }))
                        }
                        required
                      />
                    </Form.Group>

                    <Form.Group controlId="updateProductQuantity">
                      <Form.Label>Quantity</Form.Label>
                      <Form.Control
                        className="my-2"
                        type="number"
                        placeholder="Enter product quantity"
                        value={selectedProduct.quantity}
                        onChange={(e) =>
                          setSelectedProduct((prevProduct) => ({
                            ...prevProduct,
                            quantity: e.target.value,
                          }))
                        }
                        required
                      />
                    </Form.Group>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleCloseModal}>
                        Close
                      </Button>
                      <Button
                        variant="primary"
                        className="red-button"
                        type="submit"
                      >
                        Save Changes
                      </Button>
                    </Modal.Footer>
                  </Form>
                </Modal.Body>
              </Modal>
            )}
          </tbody>
        </Table>
      </Col>
      <Row xs={12} md={3} className="text-center">
        <Col className="ms-auto mt-5">
          <Card className="add-product-card">
            <Card.Header className="add-product-header">
              <Button
                variant="link"
                onClick={toggleCollapse}
                aria-expanded={!isCollapsed}
                className="text-white text-decoration-none"
              >
                {isCollapsed ? "Add Product" : "Hide Form"}
              </Button>
            </Card.Header>
            {!isCollapsed && (
              <Form className="p-3" onSubmit={(e) => newProduct(e)}>
                <Form.Group controlId="productName">
                  <Form.Control
                    className="my-2"
                    type="text"
                    value={productName}
                    onChange={(e) => setProductName(e.target.value)}
                    placeholder="Enter product name"
                    required
                  />
                </Form.Group>
                <Form.Group controlId="productDescription">
                  <Form.Control
                    className="my-2"
                    as="textarea"
                    rows={3}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="Enter product description"
                    required
                  />
                </Form.Group>

                <Form.Group controlId="productPrice">
                  <Form.Control
                    className="my-2"
                    type="number"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    placeholder="Enter product price"
                    required
                  />
                </Form.Group>

                <Form.Group controlId="productQuantity">
                  <Form.Control
                    className="my-2"
                    type="number"
                    value={quantity}
                    onChange={(e) => setQuantity(e.target.value)}
                    placeholder="Enter product quantity"
                    required
                  />
                </Form.Group>

                <Form.Group controlId="productImage">
                  <Form.Control
                    ref={fileInputRef}
                    className="my-2"
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    required
                  />
                </Form.Group>

                {isActive ? (
                  <Button type="submit" className="red-button my-2">
                    Add Product
                  </Button>
                ) : (
                  <Button type="submit" className="red-button my-2" disabled>
                    Add Product
                  </Button>
                )}
              </Form>
            )}
          </Card>
        </Col>
      </Row>
    </Row>
  );
}

export default AllProducts;
