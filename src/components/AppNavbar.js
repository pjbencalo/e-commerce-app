import { useContext } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

import "./Components.css";

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Container className="d-flex justify-content-center">
      <Navbar className="navbar-color px-5 rounded" expand="md">
        <Navbar.Brand as={NavLink} to="/" className="nav-brand-color">
          TOUCH I T
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav custom-toggler" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" className="nav-color">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/about" className="nav-color">
              About
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="nav-color ">
              Shop
            </Nav.Link>
            {user.isAdmin ? (
              <NavDropdown
                title={<span className="nav-color">Admin Menu</span>}
                className="my-dropdown"
              >
                <NavDropdown.Item
                  as={Link}
                  to="/products/all"
                  className="nav-color"
                >
                  Products
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/orders" className="nav-color">
                  Orders
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/users" className="nav-color">
                  Users
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <Nav.Link as={NavLink} to="/cart" className="nav-color">
                Cart
              </Nav.Link>
            )}

            <NavDropdown
              title={<span className="nav-color">Account</span>}
              className="my-dropdown"
            >
              {user.id ? (
                <>
                  <NavDropdown.Item
                    as={Link}
                    to="/myAccount"
                    className="nav-color px-3"
                  >
                    My Account
                  </NavDropdown.Item>
                  {user.isAdmin ? null : (
                    <NavDropdown.Item
                      as={Link}
                      to="/myOrders"
                      className="nav-color px-3"
                    >
                      My Orders
                    </NavDropdown.Item>
                  )}
                  <NavDropdown.Item
                    as={Link}
                    to="/logout"
                    className="nav-color px-3"
                  >
                    Logout
                  </NavDropdown.Item>
                </>
              ) : (
                <>
                  <NavDropdown.Item
                    as={Link}
                    to="/login"
                    className="nav-color px-3"
                  >
                    Login
                  </NavDropdown.Item>
                  <NavDropdown.Item
                    as={Link}
                    to="/register"
                    className="nav-color px-3"
                  >
                    Register
                  </NavDropdown.Item>
                </>
              )}
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </Container>
  );
}

export default AppNavbar;
