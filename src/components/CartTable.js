import { useContext, useState } from "react";
import { Button, Row, Col, Table, Modal, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";

import { ReactComponent as TrashIcon } from "bootstrap-icons/icons/trash.svg";
import { ReactComponent as EditIcon } from "bootstrap-icons/icons/pencil.svg";

import Swal from "sweetalert2";

import "./Components.css";

export default function CartTable({ products: initialProducts, handleDelete }) {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  // used for updating quantity
  const [editQuantity, setEditQuantity] = useState(null);
  const [productId, setProductId] = useState(null);
  const [showModal, setShowModal] = useState(false);
  // Managing products
  const [products, setProducts] = useState(initialProducts);

  // edit quantity
  function handleEditQuantity(productId) {
    // find product id
    const product = products.find((p) => p.productId === productId);

    if (product) {
      setEditQuantity(product.quantity);
      setProductId(product.productId);
      setShowModal(true);
    }
  }

  function handleCloseModal() {
    setShowModal(false);
  }

  function handleSaveQuantity() {
    fetch(`${process.env.REACT_APP_API_URL}/carts/updateQuantity`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: editQuantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          const updatedProducts = products.map((product) => {
            if (product.productId === productId) {
              const subtotal = product.price * editQuantity;
              return { ...product, quantity: editQuantity, subtotal: subtotal };
            }
            return product;
          });

          // Update the products array state with the updatedProducts array
          setProducts(updatedProducts);

          // Calculate the new total amount based on the updated quantities
          const newTotalAmount = updatedProducts.reduce(
            (total, product) => total + product.subtotal,
            0
          );

          // Update the total amount in the user context
          setUser((prevUser) => ({
            ...prevUser,
            totalAmount: newTotalAmount,
          }));
        } else {
          Swal.fire({
            icon: "error",
            title: "Invalid Quantity",
            text: "The entered quantity exceeds the available quantity of the product.",
          });
        }
      })
      .catch((error) => {});

    setShowModal(false);
  }

  // Delete product from cart function
  function deleteProduct(productId) {
    const updatedProducts = products.filter(
      (product) => product.productId !== productId
    );
    setProducts(updatedProducts);

    // New total amount
    const newTotalAmount = updatedProducts.reduce(
      (total, product) => total + product.subtotal,
      0
    );

    // Update the total amount in the user context
    setUser((prevUser) => ({
      ...prevUser,
      totalAmount: newTotalAmount,
    }));

    handleDelete(productId);
  }

  function checkOut() {
    fetch(`${process.env.REACT_APP_API_URL}/carts/orderCart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        products: products,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Congratulations! You have successfully ordered",
            text: "Please come again next time.",
          });

          navigate("/myOrders");
        } else {
          Swal.fire({
            icon: "error",
            title: "Something went wrong! Please try again later",
          });
        }
      })
      .catch((error) => {});
  }

  return (
    <Row className="row-cols-12 min-vh-100">
      <Col className="mt-5 text-center">
        <h1 className="m-5 cart-title">Cart</h1>
        <Table striped="columns" className="table-dark">
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Subtotal</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr key={product.productId}>
                <th className="product-name">{product.name}</th>
                <td className="col-sm-2">&#8369; {product.price}</td>
                <td className="col-sm-1">{product.quantity}</td>
                <td className="col-sm-2">&#8369; {product.subtotal}</td>
                <td className="col-sm-1">
                  <div className="actions">
                    <EditIcon
                      className="edit-icon mx-2"
                      cursor="pointer"
                      onClick={() => handleEditQuantity(product.productId)}
                    />
                    <TrashIcon
                      className="delete-icon mx-2"
                      cursor="pointer"
                      onClick={() => deleteProduct(product.productId)}
                    />
                  </div>
                </td>
              </tr>
            ))}
            <tr>
              <th>Total Amount</th>
              <td>-</td>
              <td>-</td>
              <th>&#8369; {user.totalAmount}</th>
              <td>-</td>
            </tr>
          </tbody>
        </Table>
        <div className="w-100 justify-content-end d-flex">
          <Button className="red-button" onClick={checkOut}>
            Checkout
          </Button>
        </div>
        <Modal show={showModal} onHide={handleCloseModal}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Quantity</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="formQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                value={editQuantity}
                onChange={(e) => setEditQuantity(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseModal}>
              Close
            </Button>
            <Button
              variant="primary"
              className="red-button"
              onClick={handleSaveQuantity}
            >
              Save
            </Button>
          </Modal.Footer>
        </Modal>
      </Col>
    </Row>
  );
}
