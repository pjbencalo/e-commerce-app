import { Button, Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./Components.css";

export default function ProductCard({ product, imageData }) {
  const { _id, name, description, price, quantity } = product;

  return (
    <Col xs={{ span: 6 }} md={{ span: 4 }} lg={{ span: 2 }} className="my-3">
      <Card className="product-card">
        <div className="product-img-wrapper">
          <Card.Img
            src={imageData}
            alt="Product Image"
            variant="top"
            className="product-img mx-auto"
          />
        </div>
        <Card.Body className="d-flex flex-column product-card-body">
          <Card.Title className="product-title text-center">{name}</Card.Title>
          <div className="h-50">
            <Card.Subtitle className="product-label text-center my-3">
              {description}
            </Card.Subtitle>
          </div>
          <Card.Text className="product-footer d-flex mt-auto justify-content-between mt-3">
            <span> &#8369; {price}</span>
            <span>{quantity} pcs</span>
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Button
            as={Link}
            to={`/products/${_id}`}
            className="w-100 red-button"
          >
            Details
          </Button>
        </Card.Footer>
      </Card>
    </Col>
  );
}
