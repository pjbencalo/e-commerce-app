import { Card, Container, Row } from "react-bootstrap";

export default function About() {
  return (
    <Row className="min-vh-100 bg-img justify-content-center align-items-center">
      <Card className="about-card col-9 mt-5">
        <Card.Body>
          <Card.Title>
            <h1>Welcome to TouchIT!</h1>
          </Card.Title>
          <Card.Subtitle>
            DISCLAIMER: This website is for capstone 3 purposes of Zuitt
            Bootcamp.
          </Card.Subtitle>
        </Card.Body>
        <Card.Body>
          <Card.Text className="text-justify">
            At TouchIT, we are passionate about technology and bringing you the
            latest gadgets and innovations to enhance your digital lifestyle. We
            understand that technology plays a crucial role in our daily lives,
            making tasks easier, improving communication, and providing
            entertainment. As technology enthusiasts ourselves, we curate a wide
            range of high-quality products, carefully selecting the best gadgets
            from leading brands in the industry. From smartphones and tablets to
            smart home devices, wearables, gaming consoles, and accessories, we
            strive to offer a comprehensive selection that caters to all your
            tech needs. We believe in providing exceptional customer service and
            a seamless shopping experience. Our team of knowledgeable experts is
            always available to assist you, offering personalized
            recommendations, answering your questions, and ensuring that you
            find the perfect tech solution for your requirements. In addition to
            offering top-notch products, we prioritize your satisfaction. We are
            committed to providing competitive prices, secure and convenient
            payment options, and fast shipping to ensure that you receive your
            new tech gadgets as quickly as possible. Whether you are a tech
            enthusiast, a professional seeking cutting-edge tools, or someone
            looking to upgrade their devices, TouchIT is your one-stop
            destination. We are here to help you stay up-to-date with the latest
            trends, discover innovative products, and make informed decisions.
            Thank you for choosing TouchIT. Join us on this exciting tech
            journey, and let's explore the possibilities together!
          </Card.Text>
        </Card.Body>
      </Card>
    </Row>
  );
}
