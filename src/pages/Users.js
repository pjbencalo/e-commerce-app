import { useContext, useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import UsersCard from "../components/UsersCard";
import Search from "../components/Search";

import UserContext from "../UserContext";

export default function Users() {
  const { user } = useContext(UserContext);

  const [users, setUsers] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
      })
      .catch((error) => {});
  }, []);

  const handleSearch = () => {
    const filteredResults = users.filter((user) =>
      user.firstName.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setSearchResults(filteredResults);
  };

  // Check if the user is logged in and isAdmin
  const isLoggedInAsAdmin = user && user.isAdmin;

  if (!isLoggedInAsAdmin) {
    return <Navigate to="/products" />;
  }

  return (
    <>
      <Row>
        <Search setSearchQuery={setSearchQuery} onSearch={handleSearch} />
      </Row>
      <Row>
        {(searchQuery.trim() !== "" ? searchResults : users).map((user) => (
          <UsersCard key={user._id} user={user} />
        ))}
      </Row>
    </>
  );
}
