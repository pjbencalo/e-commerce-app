import { Col, Form, Button } from "react-bootstrap";
import "./Components.css";

export default function Search({ searchQuery, setSearchQuery, onSearch }) {
  const handleSearch = (e) => {
    e.preventDefault();
    onSearch();
  };

  return (
    <Col sm={4} className="search-bar ms-auto">
      <Form className="d-flex" onSubmit={(e) => handleSearch(e)}>
        <Form.Control
          type="search"
          placeholder="Search"
          className="me-2 rounded-pill"
          aria-label="Search"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <Button type="submit" className="rounded-pill red-button">
          Search
        </Button>
      </Form>
    </Col>
  );
}
