import { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import Search from "../components/Search";

export default function Products() {
  const [products, setProducts] = useState([]);
  const [imageDataMap, setImageDataMap] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const [isLoading, setIsLoading] = useState(true); // Add isLoading state

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
        setIsLoading(false); // Set isLoading to false when data is fetched
      })
      .catch((error) => {
        setIsLoading(false); // Set isLoading to false in case of error
      });
  }, []);

  useEffect(() => {
    const fetchImageData = async (productId) => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/products/${productId}/image`
        );
        const blob = await response.blob();
        const reader = new FileReader();
        reader.onloadend = () => {
          const base64data = reader.result;
          setImageDataMap((prevImageDataMap) => ({
            ...prevImageDataMap,
            [productId]: base64data,
          }));
        };
        reader.readAsDataURL(blob);
      } catch (error) {
        console.error("Error fetching image:", error);
      }
    };

    products.forEach((product) => {
      fetchImageData(product._id);
    });
  }, [products]);

  const handleSearch = () => {
    const filteredResults = products.filter((product) =>
      product.name.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setSearchResults(filteredResults);
  };

  if (isLoading) {
    // Render the loading spinner while data is being fetched
    return (
      <div className="min-vh-100 d-flex justify-content-center align-items-center">
        <div className="spinner-border red-spinner" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    );
  }

  // Show latest
  const reversedProducts =
    products && products.length > 0 ? [...products].reverse() : [];

  return (
    <>
      <Row>
        <Search setSearchQuery={setSearchQuery} onSearch={handleSearch} />
      </Row>
      <Row>
        {(searchQuery.trim() !== "" ? searchResults : reversedProducts).map(
          (product) => (
            <ProductCard
              key={product._id}
              product={product}
              imageData={imageDataMap[product._id]}
            />
          )
        )}
      </Row>
    </>
  );
}
