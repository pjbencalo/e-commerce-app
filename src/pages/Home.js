import { Row } from "react-bootstrap";

export default function Home() {
  return (
    <Row className="min-vh-100 align-items-center body-home">
      <p aria-label="CodePen" class="p-home">
        <span data-text="T">T</span>
        <span data-text="O">O</span>
        <span data-text="U">U</span>
        <span data-text="C">C</span>
        <span data-text="H">H</span>
        <span data-text="I">I</span>
        <span data-text="T">T</span>
      </p>
    </Row>
  );
}
