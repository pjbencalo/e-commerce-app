import { ReactComponent as BackIcon } from "bootstrap-icons/icons/arrow-left.svg";
import { useContext, useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  FormControl,
  InputGroup,
  Row,
} from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";
import "./Pages.css";

export default function ProductView() {
  // getting the parameter id
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [product, setProduct] = useState({});
  const [imageDataMap, setImageDataMap] = useState({});
  const [quantityValue, setQuantityValue] = useState(1);

  //   Quantity
  const handleIncrease = () => {
    setQuantityValue((prevValue) => Math.min(prevValue + 1, product.quantity));
  };

  const handleDecrease = () => {
    setQuantityValue((prevValue) => Math.max(prevValue - 1, 1));
  };

  //   Checkout product
  const checkout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        id: productId,
        name: product.name,
        quantity: quantityValue,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: `${product.name} checked out`,
          });
          setQuantityValue(1);
          navigate("/myOrders");
        } else {
          Swal.fire({
            icon: "error",
            title: "Something went wrong!",
          });
        }
      })
      .catch((error) => {});
  };

  //   Add to cart
  const addToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        id: productId,
        quantity: quantityValue,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Added to cart!",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Product is already in the cart!",
          });
        }
      })
      .catch((error) => {
        console.error("Error adding item to cart:", error);
      });
  };

  //   Fetch product that has been click from product card
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      })
      .catch((error) => {});
  }, [productId]);

  //   fetch the product image
  useEffect(() => {
    const fetchImageData = async (productId) => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/products/${productId}/image`
        );
        const blob = await response.blob();
        const reader = new FileReader();
        reader.onloadend = () => {
          const base64data = reader.result;
          setImageDataMap((prevImageDataMap) => ({
            ...prevImageDataMap,
            [productId]: base64data,
          }));
        };
        reader.readAsDataURL(blob);
      } catch (error) {
        console.error("Error fetching image:", error);
      }
    };

    if (productId) {
      fetchImageData(productId);
    }
  }, [productId]);

  return (
    <Row className="min-vh-100">
      <Col md={{ span: 12 }} className="product-view">
        <Card className="p-5 product-view-card">
          <Link to="/products">
            <BackIcon className="back-icon" />
          </Link>
          <Row>
            <Col
              md={6}
              className="d-flex justify-content-center align-items-center"
            >
              <Card.Img
                src={imageDataMap[productId]}
                alt="Product Image"
                variant="top"
                className="product-view-img rounded"
              />
            </Col>
            <Col md={6}>
              <Card.Body>
                <Card.Title className="mb-3 text-white">
                  Name: {product.name}
                </Card.Title>
                <Card.Title className="my-3 text-white">
                  Description: {product.description}
                </Card.Title>
                <Card.Title className="my-3 text-white">
                  Price: &#8369; {product.price}
                </Card.Title>
                <Card.Title className="my-3 text-white">
                  Quantity: {product.quantity} pcs
                </Card.Title>
                <InputGroup className="my-3 quantity-handler">
                  <Button variant="outline-secondary" onClick={handleDecrease}>
                    -
                  </Button>
                  <FormControl
                    type="text"
                    value={quantityValue}
                    disabled
                    onChange={(e) => setQuantityValue(parseInt(e.target.value))}
                  />
                  <Button variant="outline-secondary" onClick={handleIncrease}>
                    +
                  </Button>
                </InputGroup>
                {!user.isAdmin ? (
                  user.id ? (
                    <>
                      <Button className="red-button me-3" onClick={addToCart}>
                        Add to Cart
                      </Button>
                      <Button className="red-button" onClick={checkout}>
                        Checkout
                      </Button>
                    </>
                  ) : (
                    <Link className="btn btn-danger btn-block" to="/login">
                      Log in here
                    </Link>
                  )
                ) : null}
              </Card.Body>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  );
}
