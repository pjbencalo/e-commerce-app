import { useState } from "react";
import { Button, Card, Col } from "react-bootstrap";
import Swal from "sweetalert2";

import "./Components.css";

export default function UsersCard({ user }) {
  const { _id, firstName, lastName, email, isAdmin } = user;
  const [isAdminState, setIsAdminState] = useState(isAdmin);

  function setAsAdmin(email) {
    fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Added to admin",
          });
          setIsAdminState(true);
        } else {
          Swal.fire({
            icon: "error",
            title: "Something went wrong! Please try again",
          });
        }
      });
  }

  function removeAsAdmin(email) {
    fetch(`${process.env.REACT_APP_API_URL}/users/removeAdmin`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Removed as admin",
          });
          setIsAdminState(false);
        } else {
          Swal.fire({
            icon: "error",
            title: "Something went wrong! Please try again",
          });
        }
      });
  }

  return (
    <Col xs={{ span: 12 }} md={{ span: 8 }} lg={{ span: 4 }} className="my-3">
      <Card className="product-card">
        <Card.Body>
          <Card.Title className="my-2">ID : {_id}</Card.Title>

          <Card.Subtitle className="my-2">
            Name: {firstName} {lastName}
          </Card.Subtitle>

          <Card.Subtitle className="my-2">Email: {email}</Card.Subtitle>

          <Card.Subtitle className="my-2">
            Admin:{" "}
            {isAdminState ? (
              <Button
                className="red-button"
                size="sm"
                onClick={() => removeAsAdmin(email)}
              >
                Disable
              </Button>
            ) : (
              <Button
                className="green-button"
                size="sm"
                onClick={() => setAsAdmin(email)}
              >
                Enable
              </Button>
            )}
          </Card.Subtitle>
        </Card.Body>
      </Card>
    </Col>
  );
}
