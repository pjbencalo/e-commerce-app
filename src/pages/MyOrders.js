import { useContext, useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function MyOrders() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [selectedOrder, setSelectedOrder] = useState(null);
  const [orders, setOrders] = useState([]);
  const [error, setError] = useState(false);

  const handleClick = (orderId) => {
    if (selectedOrder === orderId) {
      setSelectedOrder(null);
    } else {
      setSelectedOrder(orderId);
    }
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
        // Select the latest order by default
        if (data.length > 0) {
          setSelectedOrder(data[data.length - 1]._id);
        }
      })
      .catch((error) => {
        setError(true);
      });
  }, []);

  const formatPurchasedDate = (dateString) => {
    const date = new Date(dateString);
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    return date.toLocaleString("en-US", options);
  };

  useEffect(() => {
    if (user.isAdmin) {
      navigate("/products");
    }
  }, [user.isAdmin, navigate]);

  if (error) {
    return <p>Error retrieving orders. Please try again later.</p>;
  }

  const reversedOrders =
    orders && orders.length > 0 ? [...orders].reverse() : [];

  return (
    <div className="order-div">
      <h1 className="text-center">Order History</h1>
      {reversedOrders.length === 0 ? (
        <div className="text-center p-2">No orders found.</div>
      ) : (
        reversedOrders.map((order) => (
          <div className="text-center p-2 inner-order-div" key={order._id}>
            <div
              onClick={() => handleClick(order._id)}
              style={{ cursor: "pointer" }}
            >
              Purchased on {formatPurchasedDate(order.purchasedOn)}
            </div>

            {selectedOrder === order._id && (
              <Table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  {order.products.length > 0 ? (
                    order.products.map((product) => (
                      <tr key={product.productId}>
                        <td>{product.name}</td>
                        <td>&#8369; {product.price}</td>
                        <td>{product.quantity}</td>
                        <td>&#8369; {product.subtotal}</td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan="4">No products found.</td>
                    </tr>
                  )}
                  <tr>
                    <th>Total Amount</th>
                    <td colSpan="2"></td>
                    <th>&#8369; {order.totalAmount}</th>
                  </tr>
                </tbody>
              </Table>
            )}
          </div>
        ))
      )}
    </div>
  );
}
