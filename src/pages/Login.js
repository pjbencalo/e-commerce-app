import { ReactComponent as EyeIcon } from "bootstrap-icons/icons/eye.svg";
import { ReactComponent as EyeSlashIcon } from "bootstrap-icons/icons/eye-slash.svg";
import { useContext, useEffect, useState } from "react";
import { Button, Card, Col, Row, Form, InputGroup } from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

import "./Pages.css";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  // toggle password convert to text
  /*   const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  }; */

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            icon: "success",
            title: "Login successful!",
            text: "Welcome to TouchIT",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Authentication failed",
            text: "Please check your login credentials and try again",
          });
        }
      })
      .catch((error) => {});
    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      })
      .catch((error) => {});
  };

  useEffect(() => {
    if (email !== "" && password != "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id ? (
    <Navigate to="/products" />
  ) : (
    <Row className="row-cols-12 row-cols-md-3 min-vh-100 align-items-center bg-img">
      <Col className="ms-auto">
        <Card className="login-card">
          <Form className="p-3" onSubmit={(e) => authenticate(e)}>
            <Form.Group>
              <Form.Label className="text-center w-100 login-label">
                <h2>Log In</h2>
              </Form.Label>
            </Form.Group>

            <Form.Group>
              <Form.Control
                className="my-3"
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group>
              <InputGroup>
                <Form.Control
                  type={showPassword ? "text" : "password"}
                  placeholder="Enter Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />

                <InputGroup.Text
                  onClick={() => setShowPassword(!showPassword)}
                  style={{ cursor: "pointer" }}
                  className="border"
                >
                  {showPassword ? <EyeSlashIcon /> : <EyeIcon />}
                </InputGroup.Text>
              </InputGroup>
            </Form.Group>
            {isActive ? (
              <Button type="submit" className="my-3 red-button w-100">
                Login
              </Button>
            ) : (
              <Button type="submit" className="my-3 red-button w-100" disabled>
                Login
              </Button>
            )}
            <div className="w-100 text-center ">
              <Form.Text>
                Don't have an account yet?{" "}
                <Link to="/register">Register here</Link>
              </Form.Text>
            </div>
          </Form>
        </Card>
      </Col>
    </Row>
  );
}
