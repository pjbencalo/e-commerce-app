import { useContext, useState, useEffect } from "react";
import { Card, Col, Row, Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
export default function MyAccount() {
  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(false);

  function updateUser(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Details updated successfully!",
          });
          setPassword1("");
          setPassword2("");
        } else {
          Swal.fire({
            icon: "error",
            title: "There seems to be a problem. Please try again!",
          });
        }
      })
      .catch((error) => {});
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
      })
      .catch((error) => {});
  }, []);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  return !user.id ? (
    <Navigate to="/products" />
  ) : (
    <Row className="row-cols-12 row-cols-md-3 min-vh-100 align-items-center bg-img">
      <Col className="ms-auto">
        <Card className="register-card">
          <Form className="p-3" onSubmit={(e) => updateUser(e)}>
            <Form.Group>
              <Form.Label className="text-center w-100 update-label">
                <h2>Update Details</h2>
              </Form.Label>
            </Form.Group>

            <Form.Group controlId="userFirstName">
              <Form.Control
                className="my-3"
                type="text"
                placeholder="Enter first name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="userLastName">
              <Form.Control
                className="my-3"
                type="text"
                placeholder="Enter last name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="userEmail">
              <Form.Control
                className="my-3"
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                disabled
                required
              />
            </Form.Group>
            <Form.Group controlId="password1">
              <Form.Control
                className="my-3"
                type="password"
                placeholder="Enter a new password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="password2">
              <Form.Control
                className="my-3"
                type="password"
                placeholder="Confirm your password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                required
              />
            </Form.Group>

            {isActive ? (
              <Button
                variant="primary"
                type="submit"
                id="submit-button"
                className="my-3 w-100 red-button"
              >
                Update
              </Button>
            ) : (
              <Button
                variant="primary"
                type="submit"
                id="submit-button"
                className="my-3 w-100 red-button"
                disabled
              >
                Update
              </Button>
            )}
          </Form>
        </Card>
      </Col>
    </Row>
  );
}
