import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Card, Col, Row } from "react-bootstrap";
import CartTable from "../components/CartTable";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Cart() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!user.id) {
      return navigate("/login");
    }

    fetch(`${process.env.REACT_APP_API_URL}/carts`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setProducts(data.products);
          setUser((prevUser) => ({
            ...prevUser, // Preserve existing user properties
            totalAmount: data.totalAmount, // Update the setUser property
          }));
        }
      })
      .catch((error) => {})
      .finally(() => {
        setLoading(false); // Set loading to false after fetching data
      });
  }, []);

  // Delete product from cart

  function handleDelete(productId) {
    fetch(`${process.env.REACT_APP_API_URL}/carts/deleteProductCart`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setProducts((prevProducts) =>
            prevProducts.filter((product) => product.productId !== productId)
          );
          Swal.fire({
            icon: "success",
            title: "Successfully Deleted!",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Something went wrong",
          });
        }
      })
      .catch((err) => {});
  }

  if (loading) {
    return null; // Render nothing or a loading spinner while loading
  }

  return products.length ? (
    <CartTable
      key={products.productId}
      products={products}
      handleDelete={handleDelete}
    />
  ) : (
    <Row className="row-cols-12 min-vh-100 align-items-center">
      <Col className="mt-5 text-center">
        <Card>
          <Card.Body>
            <Card.Title>No items in the cart.</Card.Title>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
